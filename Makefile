###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01e - Crazy Cat Lady - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Crazy Cat Lady Program
###
### @author  Justin Erece <erece8@hawaii.edu>
### @date    17 Janaury 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = crazyCatLady

all: $(TARGET)

crazyCatLady: crazyCatLady.c
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatLady.c

clean:
	rm -f $(TARGET) *.o

