///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady catName
///
/// Result:
///   Oooooh! [catName] you’re so cute!
///
/// Example:
///   $ ./crazyCatLady Snuggles
///   Oooooh! Snuggles you’re so cute!
///
/// @author  Justin Erece <erece8@hawaii.edu>
/// @date    17 January 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <stdio.h>
#include <string>
using namespace std;

int main( int argc, char* argv[] ) {
	std::cout << "Crazy Cat Lady!" << std::endl ;
   string mystr;



   //Prompt user to input cat name
   cout << "Enter a cat name\n";

   //Get name input from the user
   getline( cin, mystr );

   //Print the name of the cat to the screen and comment that it is cute
   cout << " Oooooh! " << mystr <<  " you're so cute! \n";


   //End program
   return 0;
}
